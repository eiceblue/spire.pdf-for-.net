Spire.PDF for .NET
================
------------------------------------------------------------------------------------------------------------------------
This repository introduces a .NET PDF library Spire.PDF for .NET

Spire.PDF for .NET is a professional .NET PDF component which enables you to generate, read, edit and manipulate PDF documents in C#, VB.NET. It can be generally applied in server-side (ASP.NET or any other environment) or with Windows Forms applications without installing Adobe Acrobat or any other external libraries.
Many features are supported in Spire.PDF for .NET. First let us have a first glance of this .NET PDF component, then, expound its detail features one by one.

Spire.PDF for .NET enables users to create PDF file from webpage(html, htm, aspx), image(jpg, jpeg, png, bmp, tiff, gif, ico etc), text and export datatable from database to PDF. It also supports to generate PDF/A-1b, PDF/x1a, 2001 Compliance, standard PDF file. Furthermore, along with Spire.Doc for .NET and Spire.XLS or .NET, it can convert Word doc/docx, Excel xls/xlsx, RTF and XML to PDF.

Read PDF File
============
------------------------------------------------------------------------------------------------------------------------

Spire.PDF for .NET allows you to extract PDF text both in plain format and special format such as it can read PDF text which is written from right to left, such as Herbrew. Images of different formats and PDF attachments also can be extracted. 

This PDF component has rich features in editing PDF file. It supports to set text format and edit PDF page in multiple ways. Here is a simple list:

Text: draw text and set text format

Image: draw PDF images and different kinds of shapes

Table: draw PDF table including simple table, nested table and image table

Watermark: add both text watermark and image watermark

Bookmark:add bookmarks and get bookmarks

Header and footer

Attachment: add attachments, read attachments and remove attachments

Hyperlink: link and anchor link

List: Simple list and multi-level list

Booklet

Action: action and action chain

Automatic Field

Manipulate PDF File
========
---------------------------------------------------------------------------------------------------------------------------

There a lot of PDF tasks can be operated by using this .NET PDF library. You can merge PDF files into one PDF as well as split a huge PDF with the given number range accordingly. Furthermore, it can protect your PDF document by encryption with both owner password and user password, create PDF digital signature. If you do not need to lock your PDF, you can decrypt it quickly. Besides, you can set PDF property, PDF template and view preference as you like. Finally, webpage, image and text all can be converted to PDF which are already referred above. Form below picture, you can view most features of editing and handling PDF document clearly.

How to Install Spire.PDF for .NET
================
------------------------------------------------------------------------------------------------------------------------

After downloading, you can install it on your system whatever your OS is. For old users who have installed the old version and want to install a new one, except Spire.Pdf dll, all the contents will overlap the old one.

How to Run Demos in Projects

As there are many projects in the pack, when you run a certain project by pressing “F5” or clicking “Debug”, please make sure that you has set this project to be the StartUp Project since the default project is the first project.

If you want to create a new project by your own, you remember to add Spire.Pdf dll and Spire.License.dll as your reference.




